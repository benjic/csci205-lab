#ifndef SAREA
#define SAREA
#include <stdio.h>

typedef struct SArea {
	int StRow;
	int StCol;
	int DValue;
	int ASize;
} SArea;

typedef struct SAreaList {
	SArea** v;
	int length;
} SAreaList;

SAreaList* SAreaList_new();
void AddSArea(SAreaList*, SArea*);

SArea* SArea_new_d();
SArea* SArea_new_p(int NRow, int NCol, int DV);
void AreaDisplay(SArea*);
void AreaListDisplay(SAreaList*);
void SaveAreaList(FILE*, SAreaList*);

#endif
