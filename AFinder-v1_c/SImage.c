#include "SImage.h"
#include "ImProcExcept.h"
#include <stdio.h>
#include <stdlib.h>

SImage* SImage_new(char *S, ImProcExcept* e) {

	int TType;
	SImage* img = malloc(sizeof(SImage));
	FILE *fd;

	fd = fopen(S, "r");

	if (fd == NULL) {
		printf("  !! Error opening given file\n");
		printf("  !!  Probable cause: wrong file name\n");
		printf(" \n");
		printf("************ Quit AFTestDriver (failure) **************\n");
		printf("*******************************************************\n");
		printf(" \n");

		exit(1);
	}

	// Header - 1st Value; NRow -- must be number > 0
	TType = fscanf(fd, "%d", &img->NRow);
	if ( TType == EOF ) {
		free(e);
		e = ImProcExcept_new(0, -1, "");
		return NULL;
	}
	if ( TType == 0 ) {
		free(e);	
		e = ImProcExcept_new(1, -1, ""); // TODO Figure out how to grab bad value
		return NULL;
	}
	if ( img->NRow < 0) {
		free(e);
		e = ImProcExcept_new(2, img->NRow, "");
		return NULL;
	}

	// Header - 2nd Value; NCol -- must be number > 0
	TType = fscanf(fd, "%d", &img->NCol);
	if ( TType == EOF ) {
		free(e);
		e = ImProcExcept_new(0, -1, "");
		return NULL;
	}
	if ( TType == 0 ) {	
		free(e);
		e = ImProcExcept_new(3, -1, ""); // TODO Figure out how to grab bad value
		return NULL;
	}
	if ( img->NCol < 0) {
		free(e);
		e = ImProcExcept_new(4, img->NCol, "");
		return NULL;
	}

	int i,j;

	img->SIm    = malloc(img->NRow * sizeof(int*));
	img->ImMask = malloc(img->NRow * sizeof(int*));
	for ( i = 0; i < img->NRow; i++ ) {
		img->SIm[i] = malloc(img->NCol * sizeof(int*));
		img->ImMask[i] = malloc(img->NCol * sizeof(int*));
	}

	for ( i = 0; i < img->NRow; i++) {
		for ( j = 0; j < img->NCol; j++ ) {
			
			TType = fscanf(fd, "%d", &img->SIm[i][j]);
			
			if ( TType == EOF ) {
				free(e);
				e = ImProcExcept_new(0, -1, "");
				return NULL;
			}
			if ( TType == 0 ) {	
				free(e);
				e = ImProcExcept_new(5, -1, ""); // TODO Figure out how to grab bad value
				return NULL;
			}
			if (  img->SIm[i][j] < 0) {
				free(e);
				e = ImProcExcept_new(6, img->SIm[i][j], "");
				return NULL;
			}
		}
	}

	return img;
}

void ImageDisplay(SImage* img) {
	
	int i,j;

	printf("----------------------------\n");
	printf("The Image (%d,%d)\n", img->NRow, img->NCol );
	printf("--------------------\n");

	for ( i = 0; i < img->NRow; i++) {
		printf("Row-%d:[ ", i);
		for ( j = 0; j < img->NCol; j++ ) {
			printf("%d ", img->SIm[i][j]);
		}
		printf("]\n");
	}

	printf("----------------------------\n");
}

void SaveImage(SImage* img, FILE *fd) {

	int i,j;

	fprintf(fd, "\nThe Image (%d,%d)\n", img->NRow, img->NCol);

	for ( i = 0; i < img->NRow; i++) {
		fprintf(fd, "Row-%d:[ ", i);
		for ( j = 0; j < img->NCol; j++ ) {
			fprintf(fd, "%d ", img->SIm[i][j]);
		}
		fprintf(fd, "]\n");
	}
}
