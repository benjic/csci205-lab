#ifndef SIMAGE
#define SIMAGE
#include <stdio.h>
#include "ImProcExcept.h"

typedef struct SImage {
	int NRow;
	int NCol;
	int **SIm;
	int **ImMask;
} SImage;

SImage* SImage_new(char *S, ImProcExcept* e);
void ImageDisplay(SImage*);
void SaveImage(SImage*, FILE*);

#endif
