#ifndef SAREALIST
#define SAREALIST
#include <stdlib.h>
#include "SArea.h"


typedef struct SAreaList {
	SArea** v;
	int length;
} SAreaList;

SAreaList* SAreaList_new();
void AddSArea(SAreaList*, SArea*);

#endif
