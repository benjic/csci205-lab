#ifndef SIMAGEPROC
#define SIMAGEPROC
#include "SImage.h"
#include "SArea.h"

typedef struct SImageProc {
	SImage* S;
} SImageProc;

SImageProc* SImageProc_new(SImage*);
SAreaList* FindAreas(SImageProc*);
#endif
