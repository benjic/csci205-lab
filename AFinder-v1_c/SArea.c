#include <stdlib.h>
#include <stdio.h>
#include "SArea.h"

SArea* SArea_new_d() {
	SArea* a = malloc(sizeof(SArea));

	a->StRow = 0;
	a->StCol = 0;
	a->DValue = 0;
	a->ASize = 0;

	return a;
}

SArea* SArea_new_p(int NRow, int NCol, int DV) {
	SArea* a = malloc(sizeof(SArea));

	a->StRow = NRow;
	a->StCol = NCol;
	a->DValue = DV;
	a->ASize = 0;

	return a;
}

void AreaDisplay(SArea* a) {
	printf("[ <%d,%d>, %d, %d ]\n",
			a->StRow,
			a->StCol,
			a->DValue,
			a->ASize);
}

void AreaListDisplay(SAreaList* V) {
	int i;
	
	printf("----------------------------\n");
	printf("%d Areas Found\n", V->length);
	printf("--------------------\n");

	for ( i = 0; i < V->length; i++ ) {
		printf("#%d ", i);
		AreaDisplay(V->v[i]);
	}
}

void SaveAreaList(FILE* fd, SAreaList* V) {
	int i;

	fprintf(fd, "\n %d Areas Found\n", V->length);
	for ( i = 0; i < V->length; i++ ) {
		fprintf(fd, "#%d [ <%d,%d> %d, %d ]\n", 
				i,
				V->v[i]->StRow,
				V->v[i]->StCol,
				V->v[i]->DValue,
				V->v[i]->ASize);
	}
}
