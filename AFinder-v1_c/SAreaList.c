#include "SArea.h"
#include <stdlib.h>

SAreaList* SAreaList_new() {
	SAreaList* list = malloc(sizeof(SAreaList));
	list->length = 0;

	return list;
}

void AddSArea(SAreaList* list, SArea* a) {
	list->length += 1;
	list->v = realloc(list->v,
			sizeof(SArea*) * list->length);
	list->v[list->length - 1] = a;
}
