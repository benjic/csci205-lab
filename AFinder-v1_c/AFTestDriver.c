/**
 * AFTestDriver: The entry point into the main program of AFinder.
 *
 * Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
#include <stdio.h>
#include <stdlib.h>
#include "SArea.h"
#include "SImage.h"
#include "SImageProc.h"
#include "ImProcExcept.h"

int main(int argc, char** argv) {

	// Setup
	SImage* SIm;
	SImageProc* SIProc;
	SAreaList* AVector;
	ImProcExcept* err = ImProcExcept_new(0,-1,""); 
	// Debug/Trace Simple Text Output File
	FILE *OTF;
	// Start Processing
	// Set up debug/trace file with a fixed name
	OTF = fopen("AFTestOutput", "w");

	if ( OTF == 0 ) {
		// Handle File IO problems 
	}

	fprintf(OTF, "==AFTest Output==");

	// Printing system header & startup info
	printf(" \n");
	printf(" \n");
	printf("*******************************************************\n");
	printf("******************* AFTestDriver **********************\n");
	printf("=======================================================\n");
	printf("The system should be invoked with one arg, giving\n");
	printf("the name of the file with the image to be processed\n");
	printf("-------------------------------------------------------\n");

	// Check setup - correct number of inputs in system invocation
	// Expecting 1 argument, an image filename
	if ( argc != 2 ) {
		printf(" !! AFTestDriver invocation problem\n");
		printf(" !!  Expecting exactly 1 input - name of Image file\n");
		printf(" !!  Got %d  args instead\n", argc);
		printf(" \n");
		printf("************* Quit AFTestDriver (failure) *************\n");
		printf("*******************************************************\n");

		return 1;
	}

	printf("Start processing - Image input file: <%s>\n", argv[1]);
	
	SIm = SImage_new(argv[1], err);

	if (SIm == NULL) {
		
		printf("  !! Image processing problem\n");
		printf("  !! %s\n", err->ErrDesc);

		// More magic
		
		printf(" \n");
		printf("************ Quit AFTestDriver (failure) **************\n");
	        printf("*******************************************************\n");
	 	printf(" \n");	
		exit(1);
	}

	// Debug --> display and save
	ImageDisplay(SIm);
	SaveImage(SIm, OTF);

	SIProc = SImageProc_new(SIm);
	AVector = FindAreas(SIProc);

	AreaListDisplay(AVector);
	SaveAreaList(OTF, AVector);

	// Normal system termination
	printf(" \n");
	printf("************** Quit AreaFinder (success) **************\n");
	printf("*******************************************************\n");
	printf(" \n");
}
