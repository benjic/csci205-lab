#include <stdlib.h>
#include "SImageProc.h"
#include "SArea.h"

SImageProc* SImageProc_new(SImage* Im) {
	SImageProc* sip = malloc(sizeof(SImageProc));
	
	sip->S = Im;

	return sip;
}

int InBounds(SImageProc* Im, int I, int J) {
	return !( ( I < 0 ) || ( J < 0 ) || ( I >= Im->S->NRow ) || ( J >= Im->S->NCol ) );
}

void FindAreaExtent(SImageProc* I, SArea* A, int Row, int Col) {
	
	I->S->ImMask[Row][Col] = 1;
	A->ASize += 1;

	if ( ( InBounds(I, Row, Col-1)) && (! I->S->ImMask[Row][Col-1])
			&& ( I->S->SIm[Row][Col-1] == A->DValue) )
		FindAreaExtent(I, A, Row, Col-1);

	if ( ( InBounds(I, Row-1, Col)) && (! I->S->ImMask[Row-1][Col])
			&& ( I->S->SIm[Row-1][Col] == A->DValue) )
		FindAreaExtent(I, A, Row-1, Col);

	if ( ( InBounds(I, Row, Col+1)) && (! I->S->ImMask[Row][Col+1])
			&& ( I->S->SIm[Row][Col+1] == A->DValue) )
		FindAreaExtent(I, A, Row, Col+1);

	if ( ( InBounds(I, Row+1, Col)) && (! I->S->ImMask[Row+1][Col])
			&& ( I->S->SIm[Row+1][Col] == A->DValue) )
		FindAreaExtent(I, A, Row+1, Col);
}

SAreaList* FindAreas(SImageProc* I) {
	SAreaList* list = malloc(sizeof(SAreaList));
	SArea* NewArea;

	int i,j;

	for ( i = 0; i < I->S->NRow; i++ ) {
		for ( j = 0; j < I->S->NCol; j++ ) {
			if ( ! I->S->ImMask[i][j]) {
				 NewArea = SArea_new_p(i, j, I->S->SIm[i][j]);
				FindAreaExtent(I, NewArea, i, j);
				AddSArea(list, NewArea);
			}
		}
	}

	return list;

}	
