#include "ImProcExcept.h"
#include <stdlib.h>

static const char* ErrorNames[] = {
	"EOF found - probable cause is missing data",
	"Non-numeric found instead of integer number of rows",
	"Number of rows must be greater than zero",
	"Non-numeric found instead of integer number of columns",
	"Number of columns must be greater than zero",
	"Data value must be an integer",
	"Data value must be greater than or equal to zero"};

ImProcExcept* ImProcExcept_new(int ENum, int NValue, char* SValue) {

	ImProcExcept* e = malloc(sizeof(ImProcExcept));

	e->ErrCode = ENum;
	e->IntValue = NValue;
	e->StrValue = SValue;
	e->ErrDesc  = ErrorNames[ENum];

	return e;
}
