#ifndef IMPROCEXCEPT
#define IMPROCEXCEPT
#define EOFError 	0
#define RowNumType 	1
#define RowNumValue	2
#define ColNumType	3
#define ColNumValue	4
#define DataType	5
#define DataVal		6




typedef struct ImProcExcept {

	int ErrCode;
	int IntValue;
	char* StrValue;
	const char* ErrDesc;
} ImProcExcept;

ImProcExcept* ImProcExcept_new(int, int, char*);
#endif
