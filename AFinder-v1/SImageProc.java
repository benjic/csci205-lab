//*****************************************************************************
// AFTestDriver: A Mini-System Illustrating "AreaFinder" for a Simple 2-D Image
//
//                   Version 1.0 - January 2014
//
//                  Copyright 2014  Ray Ford
//          
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    A copy of the GNU General Public License is included with the 
//    distribution of ExFSM1.  Or see <http://www.gnu.org/licenses/>.
//
//         For questions contact ray.ford@umontana.edu
//   Computer Science, University of Montana, Missoula MT  59812
//*****************************************************************************

//*****************************************************************************
//SImageProc: encapsulates (minimal set of) image processing functions
//*****************************************************************************

//Java System Dependencies
import java.io.*;
import java.util.Vector;

//Locally Defined Dependencies
//  Exception Classes:  none
//  Other Classes:     SImage, SArea

class SImageProc
  {//Simple Image Processing Ftns on images defined in SImage
   //
   //INCLUDES an ImageMask array (of bool) for use
   //  in FindAreas and other Image processing/manipulation

   //Image State Variables
   SImage       S;          //Image + Image Mask
              
   //Simple constructor 
   SImageProc (SImage Im)  {S = Im; } 

   //Private Methods
   private boolean InBounds(int I, int J) 
     {if ( (I<0) || (J<0) || (I>=S.NRow) || (J>=S.NCol) ) return false;
      return true;
     } // end InBounds

   private SArea FindAreaExtent(SArea A, int Row, int Col) 
     {//Find areas using simple recursive algorithm and ImageMask
      //add this pixel to area A
      S.ImMask[Row][Col] = true;
      A.ASize = A.ASize + 1;
      //Use NEWS to check neighbors for inclusion in area A
      // Check Left/west
      if ( (InBounds(Row,Col-1)) && (! S.ImMask[Row][Col-1]) 
              && (S.SIm[Row][Col-1] == A.DValue) )
        //add this pixel and look at its neighbors
        FindAreaExtent(A,Row,Col-1);  //recursive call
        // end if -- end Left/west

      // Check Up/north
      if ( (InBounds(Row-1,Col)) && (! S.ImMask[Row-1][Col])
            && (S.SIm[Row-1][Col] == A.DValue) ) 
        //add this pixel and look at its neighbors
        FindAreaExtent(A,Row-1,Col);  //recursive call
        // end if -- end Up/north

      // Check Right/east
      if ( (InBounds(Row,Col+1)) && (! S.ImMask[Row][Col+1])
             && (S.SIm[Row][Col+1] == A.DValue) )
        //add this pixel and look at its neighbors
        FindAreaExtent(A,Row,Col+1);  //recursive call
        // end if -- end Right/east

      // Check Down/south
      if ( (InBounds(Row+1,Col)) && (! S.ImMask[Row+1][Col])
             && (S.SIm[Row+1][Col] == A.DValue) ) 
        //add this pixel and look at its neighbors
        FindAreaExtent(A,Row+1,Col);  //recursive call
        // end if
      return A;
      }  //end FindAreaExtent

   //Public Method
   public Vector FindAreas() 
     {//Setup, then use recursive FindAreaExtent to find areas
      //Setup
      Vector      AreaList = new Vector();
      SArea       NewArea;      

      //Check all pixels, in row-wise order
      for (int I=0; I < S.NRow; I++)
        for (int J = 0; J < S.NCol; J++)
          {if (! S.ImMask[I][J])  
             {//pixel NOT yet assigned to an Area, so do it now!
              NewArea = new SArea(I,J,S.SIm[I][J]);
              FindAreaExtent(NewArea,I,J);
              AreaList.addElement(NewArea);    //object
              //add NewArea to AreaList vector
             }  // end if
           } // end for J ...
        // end for I ...

      // reached here ==> done with Area Finding
      return AreaList;
     }  //end FindAreas

  }  //end SImageProc
