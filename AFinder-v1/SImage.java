//*****************************************************************************
// AFTestDriver: A Mini-System Illustrating "AreaFinder" for a Simple 2-D Image
//
//                   Version 1.0 - January 2014
//
//                  Copyright 2014  Ray Ford
//          
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    A copy of the GNU General Public License is included with the 
//    distribution of ExFSM1.  Or see <http://www.gnu.org/licenses/>.
//
//         For questions contact ray.ford@umontana.edu
//   Computer Science, University of Montana, Missoula MT  59812
//*****************************************************************************

//*****************************************************************************
//SImage: defines a simple image object with basic capabilities
//*****************************************************************************

//Java System Dependencies
import java.io.*;

//Local Dependencies
//  Exception Classes: ImProcExcept 

class SImage
  {//Simple image, defined here as 2-D array [NRow,NCol] of int
   //
   //Also includes an ImageMask (of bool) for use
   //  in FindAreas and other Image processing/manipulation

   //Image State Variables
   int         NRow;            //Number of rows
   int         NCol;            //Number of columns
   int[][]     SIm;             //Actual Image
   Boolean[][] ImMask;          //Image mask
        
   //Image constructor and initialization - option implemented here is to read the
   //  image from an external file name, with name specified by the input string
   SImage (String S) throws FileNotFoundException, IOException, ImProcExcept
     {//Use StreamTokenizer to process the input file as a sequence of int values
      //  
      //Exceptions: StreamTokenizer throws standard exceptions for wrong file name 
      //  (file named not found in current directory) and unexpected end of file 
      //  (usually caused by missing data). SimpImage explicitly throws locally defined
      //  exceptions of class ImProcExcept for other input file/data problems
      // 
      //All exceptions thrown here are currently caught by and reported to the user
      //   in the calle, the main pgm AreaFinderSys

      FileReader      IFile;
      StreamTokenizer Tkns;
      int             TType;
      int             Temp;   //temporary value holder
      IFile  = new FileReader(S);   //bad name ==> exception thrown
      Tkns   = new StreamTokenizer(IFile);
      //Process file

      //Header - 1st Value: NRow -- must be number > 0
      TType  = Tkns.nextToken();
      if (TType == StreamTokenizer.TT_EOF)
        throw new ImProcExcept(0,-1,"");
      if (TType != StreamTokenizer.TT_NUMBER) 
        throw new ImProcExcept(1,-1,Tkns.sval);
      NRow = (int) Tkns.nval;                  //double --> int
      if (NRow <= 0) 
        throw new ImProcExcept(2,NRow,"");

      //Header - 2nd Value: NCol -- must be number > 0
      TType  = Tkns.nextToken();
      if (TType == StreamTokenizer.TT_EOF)
        throw new ImProcExcept(0,-1,"");
      if (TType != StreamTokenizer.TT_NUMBER) 
        throw new ImProcExcept(3,-1,Tkns.sval);
      NCol = (int) Tkns.nval;                  //double --> int
      if (NCol <= 0) 
        throw new ImProcExcept(4,NCol,"");

      //Legal Header ==> Create empty image, then read values
      SIm    = new int[NRow][NCol];
      ImMask = new Boolean[NRow][NCol];
      for (int I=0; I < NRow; I++)
        for (int J = 0; J < NCol; J++)
          {TType  = Tkns.nextToken();
           if (TType == StreamTokenizer.TT_EOF)
             throw new ImProcExcept(0,-1,"");
           if (TType != StreamTokenizer.TT_NUMBER)
             throw new ImProcExcept(5,-1,Tkns.sval);
           Temp = (int) Tkns.nval;                  //double --> int
           if (Temp >= 0) 
             {SIm[I][J] = Temp;
              ImMask[I][J] = false;
             } 
           else throw new ImProcExcept(6,Temp,"");
           // end if/then/else
          } // end for J ...
        // end for I ...

      //reached here ==> finished processing input file - initialization done    
     }  //end constructor

   //Public methods
   public void ImageDisplay()
     {//print Image values to command line display
      System.out.println("----------------------------");
      System.out.println("The Image (" + NRow + "," + NCol + ")");
      System.out.println("--------------------");
      for (int I = 0; I < NRow; I++)
        {System.out.print("Row-" + I + ":[ ");
         for (int J = 0; J < NCol; J++)
           System.out.print(SIm[I][J] + " ");
         System.out.println("]");
        }  // end for
      System.out.println("----------------------------");
     }  //end ImageDisplay 

   public void SaveImage(FileWriter OTF) throws IOException
     {//save image as test to specified file
      char  NewLine = (char) 10;
      OTF.write(NewLine + "The Image (" + NRow + "," + NCol + ")");
      for (int I = 0; I < NRow; I++)
        {OTF.write(NewLine + "Row-" + I + ":[ ");
         for (int J = 0; J < NCol; J++)
           OTF.write(SIm[I][J] + " ");
         OTF.write("]");
        }  // end for
       OTF.write(NewLine);
     }  //end SaveImage 

  }  //end SImage
