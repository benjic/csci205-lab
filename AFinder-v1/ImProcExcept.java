//*****************************************************************************
// AFTestDriver: A Mini-System Illustrating "AreaFinder" for a Simple 2-D Image
//
//                   Version 1.0 - January 2014
//
//                  Copyright 2014  Ray Ford
//          
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    A copy of the GNU General Public License is included with the 
//    distribution of ExFSM1.  Or see <http://www.gnu.org/licenses/>.
//
//         For questions contact ray.ford@umontana.edu
//   Computer Science, University of Montana, Missoula MT  59812
//*****************************************************************************

//*****************************************************************************
//ImProcExcept: defines a class of AFTestDriver-specific exceptions
//*****************************************************************************

//Java System Dependencies
//  Exception, from std Java library

//Other Locally Defined Dependencies
//  none

public class ImProcExcept  extends Exception
  {//Defines exceptions for input file errors detected by in-code checking
   //  Note that "wrong file name" and "end of file found" (missing data)
   //  are detected by StreamTokenizer facilities, which throw standard 
   //  exceptions (FileNotFound and IOException)
   int     ErrCode;
   int     IntValue;
   String  StrValue;
   String  ErrDesc;

   //Local Error Codes
   final static int EOFError        = 0;
   final static int RowNumType      = 1;
   final static int RowNumValue     = 2;
   final static int ColNumType      = 3;    
   final static int ColNumValue     = 4;
   final static int DataType        = 5;
   final static int DataVal         = 6;
  
   //String rep for Error Codes
   final static String[] ErrorNames =
     {"EOF found - probable cause is missing data",
      "Non-numeric found instead of integer number of rows",
      "Number of rows must be greater than zero",       
      "Non-numeric found instead of integer number of columns", 
      "Number of columns must be greater than zero",  
      "Data value must be an integer",
      "Data value must be greater than or equal to zero"}; 

   //Constructor
   ImProcExcept (int ENum, int NValue, String SValue) 
     {ErrCode = ENum; IntValue = NValue; StrValue = SValue;  
      ErrDesc = ErrorNames[ENum]; } 
   
  }  //end ImProcExcept
