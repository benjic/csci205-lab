//*****************************************************************************
// AFTestDriver: A Mini-System Illustrating "AreaFinder" for a Simple 2-D Image
//    for more details about AFTestDriver: see CSCI-205 Spring 2014 Notes
//
//                   Version 1.0 - January 2014
//
//                  Copyright   2014  Ray Ford
//          
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    A copy of the GNU General Public License is included with the 
//    distribution of ExFSM1.  Or see <http://www.gnu.org/licenses/>.
//
//         For questions contact ray.ford@umontana.edu
//   Computer Science, University of Montana, Missoula MT  59812
//*****************************************************************************

//*****************************************************************************
//AFTestDriver: the "main pgm", a dummy/test driver to exercise SImageProc
//*****************************************************************************

//Java System Dependencies
import java.io.*;
import java.util.Vector;

//Locally Defined Dependencies
//  Exception Classes: ImProcExcept 
//  Other Classes:     SImage, SArea, SImageProc

class AFTestDriver
  {public static void main (String [] args)  throws IOException
    {//setup
     SArea            TempA;
     SImage           SIm;
     SImageProc       SIProc;
     Vector           AVector;
     //Debug/Trace Simple Text Output File
     FileWriter       OTF = null;

     try   //start processing
       {//set up debug/trace file with fixed name
        OTF = new FileWriter("AFTestOutput"); 
        OTF.write("==AFTest Output==");       // YES  

        //Display system header & startup info
        System.out.println(" ");
        System.out.println(" ");
        System.out.println("*******************************************************");
        System.out.println("******************* AFTestDriver **********************");
        System.out.println("=======================================================");
        System.out.println("The system should be invoked with one arg, giving");
        System.out.println("the name of the file with the image to be processed");
        System.out.println("-------------------------------------------------------");
        //Check setup  - correct number of inputs in system invocation
        // Expect 1 arg, input image filename 
        if (args.length != 1)
          {System.out.println(" !! AFTestDriver invocation problem"); 
           System.out.println(" !!  Expecting exactly 1 input - name of Image file");  
           System.out.println(" !!  Got " + args.length + " args instead");
           System.out.println(" ");
           System.out.println("************* Quit AFTestDriver (failure) *************");
           System.out.println("*******************************************************");      
           return;
          }; 

        //Main pgm core - will process correctly OR throw an I/O exception
        System.out.println("Start processing - Image input file: <" + args[0] + ">");

        //Create image object, read input file to create the image 
        SIm = new SImage(args[0]);
//Debug --> display & save
SIm.ImageDisplay();
SIm.SaveImage(OTF);

        //Create SImageProc object, create SArea object, find areas 
        SIProc = new SImageProc(SIm);
        AVector = SIProc.FindAreas();
//Debug --> display & save
TempA  = new SArea();
TempA.AreaListDisplay(AVector);
TempA.SaveAreaList(OTF,AVector);

        //Normal system termination
        System.out.println(" ");
        System.out.println("************** Quit AreaFinder (success) **************");
        System.out.println("*******************************************************");
        System.out.println(" "); 
        OTF.close();                        
        return;
       }  //end try

    //Abnormal terminations -- file or Image problems
    catch (FileNotFoundException  FNFE)
      {System.out.println("  !! InputFileNotFound Exception");
       System.out.println("  !!  Probable cause: wrong file name");
       System.out.println(" ");
       System.out.println("************ Quit AFTestDriver (failure) **************");
       System.out.println("*******************************************************");   
       System.out.println(" ");   
      }  //  end catch
    catch (IOException  IOE)
      {System.out.println("  !! Image input problem --> I/O Exception");
       System.out.println("  !!  Probable cause: corrupted file");
       System.out.println(" ");
       System.out.println("************ Quit AFTestDriver (failure) **************");
       System.out.println("*******************************************************");    
       System.out.println(" "); 
      }  //  end catch
    catch (ImProcExcept  IPE)
      {System.out.println("  !! Image processing problem");
       System.out.println("  !! " + IPE.ErrDesc);
       switch (IPE.ErrCode)
         {case 0: break;
          case 1: case 3: case 5: 
            System.out.println("  !!  Value Found:<" + IPE.StrValue + ">");
            break;
          case 2: case 4: case 6: 
            System.out.println("  !!  Value Found:<" + IPE.IntValue + ">");
         }  // end case  
       System.out.println(" ");
       System.out.println("************ Quit AFTestDriver (failure) **************");
       System.out.println("*******************************************************"); 
       System.out.println(" "); 
      }  // end catch
    finally 
      {if (OTF != null) OTF.close();
      }   //end finally 
    // end try/catch

    }  //end main
  }  //end class AFTestDriver
