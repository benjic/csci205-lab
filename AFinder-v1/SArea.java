//*****************************************************************************
// AFTestDriver: A Mini-System Illustrating "AreaFinder" for a Simple 2-D Image
//
//                   Version 1.0 - January 2014
//
//                  Copyright   2014  Ray Ford
//          
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    A copy of the GNU General Public License is included with the 
//    distribution of ExFSM1.  Or see <http://www.gnu.org/licenses/>.
//
//         For questions contact ray.ford@umontana.edu
//   Computer Science, University of Montana, Missoula MT  59812
//*****************************************************************************

//*****************************************************************************
//SArea: defines a minimal area descriptor with basic capabilities
//*****************************************************************************

//Java System Dependencies
import java.io.*;
import java.util.Vector;

//Local Dependencies
//  none 

class SArea
  {//Simple/Minimal Image Area Descriptor, containing ONLY
   // <StRow, StCol>  Row & Column numbers of the upper-most/left-most pixel
   //                   in the area
   //  DValue          Data value shared by all pixels in the area
   //  ASize           Number of pixels in the area
   //  
   //Note that this descriptor does NOT attempt to "remember" all the pixels
   //  in an area, e.g. by using (complex, dynamic) list data structures. 
   //  Instead, it assumes that if it is necessary to (re)find the pixels in
   //  an area this can be done effectively and efficiently using StRow & StCol. 

   //Area descriptor elements 
   int        StRow;               //Upper-Most row  
   int        StCol;               //Left-Most column on Upper-Most row
   int        DValue;              //Image data value          
   int        ASize;               //Area size (in pixels)
   //Option for debug/trace
//   FileWriter DebugFile; 
   
   //Area (descriptor)constructors
   //  Option: create simple null descriptor
   SArea ( ) 
     {//all attributes set to zero by default   
      StRow = 0;  StCol = 0; DValue = 0; ASize = 0;    
     }  //end constructor

//   SArea (FileWriter OTF) 
//     {//set trace file - other attributes set to zero by default   
//      DebugFile = OTF;    
//     }  //end constructor

   //  Option: create descriptor from arg values
   SArea (int NRow, int NCol, int DV)
     {//Start Pt and DataValue set by input   
      StRow = NRow;  StCol = NCol; DValue = DV; 
      //Initial ASize is zero
      ASize = 0;    
     }  //end constructor

   //Public Methods
   public void AreaDisplay()
     {//display an area descriptor
      System.out.println("[ <"+StRow+","+StCol+">, "+DValue+", "+ASize+" ]");
     }  // end AreaDisplay

   public void AreaListDisplay(Vector V)
     {//display list of area descriptors
      SArea TempA;
      System.out.println("----------------------------");
      System.out.println(V.size() + " Areas Found");
      System.out.println("--------------------");
      for (int I = 0; I < V.size(); I++)
        {TempA = (SArea) V.elementAt(I);
         System.out.print("#" + I + " ");
         TempA.AreaDisplay();
        }  // end for
     }  //end AreaListDisplay 

   public void SaveAreaList(FileWriter OTF, Vector V) throws IOException
     {//save list of area descriptors to specified file
      char  NewLine = (char) 10;   //uni-code for carriage return
      SArea Temp;
      OTF.write(NewLine + " " + V.size() + " Areas Found" + NewLine);
      for (int I = 0; I < V.size(); I++)
        {Temp = (SArea) V.elementAt(I);
         OTF.write("#"+I+" "+"[ <"+Temp.StRow+","+Temp.StCol+">, "
                     +Temp.DValue+", "+Temp.ASize+" ]" + NewLine);
        }  // end for

     }  //end SaveAreaList 

  }  //end SArea
