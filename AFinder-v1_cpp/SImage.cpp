#include "AFinder.hpp"
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

SImage::SImage(char* S) {

	int TType;
	int Temp;

	std::ifstream IFile(S);
	std::string line;

	// Header
	std::getline(IFile, line);
	std::stringstream ss(line);

	if ( ! (ss >> NRow) ) {
		if ( ss.eof() )
			throw ImProcException(0, -1, "");
		else
			throw ImProcException(1, -1, "");
	}

	if ( NRow <= 0 ) 
		throw ImProcException(2, NRow, "");

	if ( ! ( ss >> NCol ) ) {
		if ( ss.eof() != 0 )
			throw ImProcException(1, -1, "");
		else
			throw ImProcException(3, -1, "");
	}

	if ( NCol <= 0 )
		throw ImProcException(4, NCol, "");

	SIm = new int*[NRow];
	ImMask = new bool*[NRow];

	for ( int i = 0; i < NRow; i++ ) {
		
		std::getline(IFile, line);
		std::stringstream ss(line);

		SIm[i] = new int[NCol]; 
		ImMask[i] = new bool[NCol];

		for ( int j = 0; j < NCol; j++ ) {
			
			if ( ! ( ss >> Temp ) ) {
				if ( ss.eof() )
					throw ImProcException(0, -1, "");
				else
					throw ImProcException(5,-1, "");
			}

			if ( Temp >= 0 ) {
				SIm[i][j] = Temp;
				ImMask[i][j] = false;
			} else {
				throw ImProcException(6, Temp, "");
			}
		}
	}
}

void SImage::ImageDisplay() {

	std::cout << "----------------------------" << std::endl;
	std::cout << "The Image(" << NRow << "," << NCol << ")" << std::endl;
	std::cout <<  "--------------------" << std::endl;

	for ( int i = 0; i < NRow; i++ ) {
		std::cout << "Row-" << i << ":[ ";
		for ( int j = 0; j < NCol; j++) {
			std::cout << SIm[i][j] << " ";
		}
		std::cout << "]" << std::endl;
	}

	std::cout << "----------------------------" << std::endl;
}

void SImage::SaveImage(std::ofstream& OTF) {
	OTF << std::endl << "The Image (" << NRow << "," << NCol << ")";

	for ( int i = 0; i < NRow; i++ ) {
		OTF << std::endl << "Row-" << i << ":[ ";
		for ( int j = 0; j < NCol; j++) {
			OTF << SIm[i][j] << " ";
		}
		OTF << "]";
	}
	OTF << std::endl << std::endl;
}
