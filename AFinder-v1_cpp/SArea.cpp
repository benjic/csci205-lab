#include "AFinder.hpp"
#include <iostream>
#include <vector>

using namespace std;

SArea::SArea() {

	StRow = 0;
	StCol = 0;
	DValue = 0;
	ASize = 0;

}

SArea::SArea(int NRow, int NCol, int DV) {

	StRow = NRow;
	StCol = NCol;
	DValue = DV;
	ASize = 0;

}

void SArea::AreaDisplay() {
	cout << "[ <" << StRow << "," << StCol << ">, " << DValue << ", " << ASize << " ]" << endl;	
}

void SArea::AreaListDisplay( std::vector<SArea> V ) {

	int count = 0;

	cout << "----------------------------" << endl;
	cout << V.size() << " Areas Found" << endl;
	cout << "----------------------------" << endl;

	for ( std::vector<SArea>::iterator it = V.begin(); it < V.end(); it++ ) {
		
		cout << "#" << count++ << " ";
		(*it).AreaDisplay();

	}
}

void SArea::SaveAreaList(std::ofstream& OTF, std::vector<SArea> V) {

	int count = 0;
	OTF << " " << V.size() << " Areas Found" << endl;

	for ( std::vector<SArea>::iterator it = V.begin(); it < V.end(); it++ ) {
		
		OTF << "#" << count++ << " [ <" << (*it).StRow << "," << (*it).StCol << ">, " 
			<< (*it).DValue << ", " << (*it).ASize << " ]" << std::endl; 
	}
}
