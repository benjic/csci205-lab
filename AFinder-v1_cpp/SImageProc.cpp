#include "AFinder.hpp"



SImageProc::SImageProc(SImage im) : S(im) {

}

bool SImageProc::InBounds(int i, int j) {
	if ( ( i < 0 ) || ( j < 0 ) || ( i >= S.NRow ) || ( j >= S.NCol ) )
		return false;

	return true;
}

SArea SImageProc::FindAreaExtent(SArea& A, int Row, int Col) {

	S.ImMask[Row][Col] = true;
	A.ASize += 1;

	if ( ( ( InBounds(Row, Col-1 ) ) && ( ! ( S.ImMask[Row][Col-1] ) )
				&& ( S.SIm[Row][Col-1] == A.DValue ) ) )
		FindAreaExtent(A, Row, Col-1);

	if ( ( ( InBounds(Row-1, Col ) ) && ( ! ( S.ImMask[Row-1][Col] ) )
				&& ( S.SIm[Row-1][Col] == A.DValue ) ) )
		FindAreaExtent(A, Row-1, Col);

	if ( ( ( InBounds(Row, Col+1 ) ) && ( ! ( S.ImMask[Row][Col+1] ) )
				&& ( S.SIm[Row][Col+1] == A.DValue ) ) )
		FindAreaExtent(A, Row, Col+1);

	if ( ( ( InBounds(Row+1, Col ) ) && ( ! ( S.ImMask[Row+1][Col] ) )
				&& ( S.SIm[Row+1][Col] == A.DValue ) ) )
		FindAreaExtent(A, Row+1, Col);
	
	return A;
}

std::vector<SArea> SImageProc::FindAreas() {
	std::vector<SArea> AreaList;
	SArea	NewArea;

	for ( int i = 0; i < S.NRow; i++) {
		for (int j = 0; j < S.NCol; j++) {
			if ( !S.ImMask[i][j] ) {
				NewArea = SArea(i,j,S.SIm[i][j]);
				FindAreaExtent(NewArea, i, j);
				AreaList.push_back(NewArea);
			}
		}
	}

	return AreaList;

}
