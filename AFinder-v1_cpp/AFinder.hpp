#include <vector>
#include <fstream>
#include <iostream>
#include <string>

class SArea {
	public:
		int StRow;
		int StCol;
		int DValue;
		int ASize;
		SArea();
		SArea(int, int, int);
		void AreaDisplay();
		void AreaListDisplay( std::vector<SArea> );
		void SaveAreaList( std::ofstream& , std::vector<SArea> );
};

class ImProcException : public std::exception {
	const static char ErrorNames[7][100];

	const static int EOFError        = 0;
	const static int RowNumType      = 1;
	const static int RowNumValue     = 2;
	const static int ColNumType      = 3;    
	const static int ColNumValue     = 4;
	const static int DataType        = 5;
	const static int DataVal         = 6;

	
  
	public:
		int ErrCode;
		int IntValue;
		char* StrValue;
		char* ErrDesc;
		ImProcException(int, int, std::string);
		virtual const char* what() const throw();
};


class SImage {
	public:
		int NRow;
		int NCol;
		int** SIm;
		bool** ImMask;
		SImage(char*);
		void ImageDisplay();
		void SaveImage(std::ofstream&);
};


class SImageProc {
	SImage S;	
	public:
		bool InBounds(int, int);
		SArea FindAreaExtent(SArea&, int, int);
		std::vector<SArea> FindAreas();
		SImageProc(SImage);
		SImageProc();
};
