#include<cstdlib>
#include "AFinder.hpp"

using namespace std;

int main( int argc, char** argv ) {

	std::vector<SArea> AVector;
	ofstream OTF;

	try {
		OTF.open("AFTestOutput");
		OTF << "==AFTest Output==";

		cout << endl << endl;
		cout << "*******************************************************" << endl;
		cout << "******************* AFTestDriver **********************" << endl;
		cout << "=======================================================" << endl;
		cout << "The system should be invoked with one arg, giving" << endl;
		cout << "the name of the file with the image to be processed" << endl;
		cout << "-------------------------------------------------------" << endl;

		if (argc != 2) {
			cout << " !! AFTestDriver invocation problem" << endl; 
			cout << " !!  Expecting exactly 1 input - name of Image file" << endl;
			cout << " !!  Got " << argc << " args instead" << endl;
			cout << " " << endl;
			cout << "************* Quit AFTestDriver (failure) *************" << endl;
			cout << "*******************************************************" << endl;
			
			return 1;
		} 
		
		cout << "Start processing - Image input file: <" << argv[1] << ">" << endl;

		SImage SIm(argv[1]);	

		SIm.ImageDisplay();
		SIm.SaveImage(OTF);

		SImageProc SIProc(SIm);
		AVector = SIProc.FindAreas();

		SArea TempA = SArea();
		TempA.AreaListDisplay(AVector);
		TempA.SaveAreaList(OTF, AVector);

		
		cout << " " << endl;
		cout << "************** Quit AreaFinder (success) **************" << endl;
		cout << "*******************************************************" << endl;
		cout << " " << endl; 

	} catch ( ImProcException& IPE ) {
		cout << "  !! Image processing problem" << endl;
		cout << "  !! " << IPE.ErrDesc << endl;

		cout << IPE.what() << endl;
	}
}
