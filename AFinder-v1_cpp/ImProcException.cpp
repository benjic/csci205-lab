#include "AFinder.hpp"
#include <string>

const char ImProcException::ErrorNames[][100] = {
	     "EOF found - probable cause is missing data",
	     "Non-numeric found instead of integer number of rows",
	     "Number of rows must be greater than zero",       
	     "Non-numeric found instead of integer number of columns", 
	     "Number of columns must be greater than zero",  
	     "Data value must be an integer",
	     "Data value must be greater than or equal to zero"
}; 

ImProcException::ImProcException(int ENum, int NValue, std::string SValue){
	ErrCode 	= ENum;
	IntValue 	= NValue;
	StrValue 	= const_cast<char*> (SValue.c_str());
	ErrDesc		= const_cast<char*> (ErrorNames[ENum]);
}

const char* ImProcException::what() const throw() {
		
	char* buffer = new char[100];

	if ( ErrCode != 0 ) {
		if ( ErrCode % 2 == 0 ) {
			sprintf(buffer, "  !! Value Found:<%s>\n", StrValue);
		} else {
			sprintf(buffer, "  !! Value Found:<%d>\n", IntValue);
		}
	}

	return const_cast<char*>(buffer);

}
