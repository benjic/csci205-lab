# Assignment 2

This a simple CPP program to meet the requirments of printing out Boat Knight's identity. Just for fun I expanded a bit and allowed you to specify an identity as well.

## Building

It is a simple program. A simple 'make' will build the executable in your current dir and 'make install' will install the binary in your PATH somewhere. If you want you can use the make unistall target to remove the single binary. You will probably need root to install which will require sudo.

## Executing

BoatKnight [options] [IDENTITY]

### Options
-h, --help A simple help message

### IDENTITY
A series of arguments describing Boat Knight

## Help
This is your help.
